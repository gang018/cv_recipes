#### Стек технологий:
* Android Studio 3
* Java 8
* MVP + Clean Architecture (https://github.com/android10/Android-CleanArchitecture)
* Dagger 2
* Retrofit 2
* RxJava 2

Приложение отображает список рецептов, по нажатию на рецепт открывается подробное описание рецепта.

![Screenshot 1](https://bytebucket.org/gang018/cv_recipes/raw/39aa43cd1484a070d50b97124a2427b1966a5d69/screenshots/Screenshot_2017-11-06-12-44-47.jpg)
![Screenshot 2](https://bytebucket.org/gang018/cv_recipes/raw/39aa43cd1484a070d50b97124a2427b1966a5d69/screenshots/Screenshot_2017-11-06-12-44-52.jpg)