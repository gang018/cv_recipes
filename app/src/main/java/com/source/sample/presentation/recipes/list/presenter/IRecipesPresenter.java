package com.source.sample.presentation.recipes.list.presenter;
import com.source.sample.data.recipes.model.Recipe;
import com.source.sample.presentation.BasePresenter;
import com.source.sample.presentation.recipes.list.view.IRecipesView;

/**
 * Created by Roman,
 * Date: 22.10.17,
 * email: gang018@mail.ru
 */

public interface IRecipesPresenter extends BasePresenter<IRecipesView> {

    void controlRecipeItemClick(final Recipe recipe);

    void controlRetryButtonClick();
}
