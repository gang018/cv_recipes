package com.source.sample.presentation.recipes.list.view.view;
import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import com.source.sample.data.recipes.model.Recipe;
import com.source.sample.data.recipes.model.RecipesList;
import com.source.sample.presentation.recipes.list.view.fragment.RecipesListFragment;
import com.source.sample.presentation.widgets.ErrorView;
import com.source.sample.utils.Constants;

/**
 * Created by: Roman Zakharov
 * Date: 24.10.2017
 * Email: gang018@mail.ru
 */

public class RecipesView extends RelativeLayout {

    private ProgressBar mProgressView;
    private ErrorView mErrorView;
    private ListView mListView;
    private RecipesListAdapter mAdapter;
    private RecipesListFragment.RecipeClickListener mClickListener;

    public RecipesView(Context context) {
        super(context);

        init();
    }

    private final void init() {
        // Progress
        mProgressView = new ProgressBar(getContext());
        mProgressView.setVisibility(INVISIBLE);
        mProgressView.setIndeterminate(true);

        final LayoutParams progressParams = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        progressParams.addRule(CENTER_IN_PARENT);
        mProgressView.setLayoutParams(progressParams);
        addView(mProgressView);


        // Error view
        mErrorView = new ErrorView(getContext());
        mErrorView.setVisibility(INVISIBLE);

        final LayoutParams errorParams = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
        errorParams.addRule(CENTER_IN_PARENT);
        mErrorView.setLayoutParams(errorParams);
        addView(mErrorView);


        // ListView with content
        mListView = new ListView(getContext());
        mListView.setAdapter(mAdapter = new RecipesListAdapter());
        mListView.setCacheColorHint(Color.TRANSPARENT);
        mListView.setVisibility(INVISIBLE);
        mListView.setFadingEdgeLength(Constants.ZERO);
        mListView.setOnItemClickListener(mInnerRecipeClickListener);

        final LayoutParams listViewParams = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
        mListView.setLayoutParams(listViewParams);
        addView(mListView);
    }

    public final void setProgressVisible(final int visibility) {
        if (mProgressView != null) {
            mProgressView.setVisibility(visibility);
        }
    }

    public final void setContentVisible(final int visibility) {
        if (mListView != null) {
            mListView.setVisibility(visibility);
        }
    }

    public final void setErrorVisible(final int visibility) {
        if (mErrorView != null) {
            mErrorView.setVisibility(visibility);
        }
    }

    public final void setRecipes(final RecipesList recipes) {
        if (mAdapter != null) {
            mAdapter.setRecipesList(recipes);
        }
    }

    public final void setOnRecipeClickListener(final RecipesListFragment.RecipeClickListener listener) {
        mClickListener = listener;
    }

    public final void setOnRetryClickListener(final OnClickListener listener) {
        if (mErrorView != null) {
            mErrorView.setOnRetryClickListener(listener);
        }
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();

        mProgressView = null;
        mListView = null;
        mInnerRecipeClickListener = null;
        mClickListener = null;
        mErrorView = null;

        if (mAdapter != null) {
            mAdapter.release();
        }
        mAdapter = null;
    }

    private AdapterView.OnItemClickListener mInnerRecipeClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
            if (mAdapter != null) {
                final Recipe recipe = mAdapter.getItem(i);
                if (mClickListener != null) {
                    mClickListener.onRecipeClick(recipe);
                }
            }
        }
    };
}