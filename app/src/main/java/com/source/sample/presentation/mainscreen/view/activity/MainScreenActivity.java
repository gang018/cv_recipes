package com.source.sample.presentation.mainscreen.view.activity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import com.source.sample.R;
import com.source.sample.di.DiManager;
import com.source.sample.presentation.mainscreen.presenter.IMainScreenPresenter;
import com.source.sample.presentation.mainscreen.view.IMainScreenView;
import com.source.sample.presentation.router.AppScreensRouter;
import javax.inject.Inject;

public class MainScreenActivity extends AppCompatActivity implements IMainScreenView {

    @Inject
    AppScreensRouter mRouter;

    @Inject
    IMainScreenPresenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        DiManager.getMainScreenComponent().inject(this);
        mRouter.setActivity(this);

        mPresenter.bindView(this);
    }

    @Override
    public void showRecipesList() {
        if (mRouter != null) {
            mRouter.openRecipesList();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (mRouter != null && !mRouter.handleBackPressed()) {
            super.onBackPressed();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (mPresenter != null) {
            mPresenter.release();
        }
        mPresenter = null;

        if (mRouter != null) {
            mRouter.release();
        }
        mRouter = null;
    }
}
