package com.source.sample.presentation.recipes.list.view.view.states;
import com.source.sample.data.recipes.model.RecipesList;
import com.source.sample.presentation.recipes.list.view.IRecipesView;

/**
 * Created by Roman,
 * Date: 05.11.17,
 * email: gang018@mail.ru
 */

class ContentState implements IRecipesListViewState {

    @Override
    public void setLoadingState(IRecipesView view) {
        // ignore
    }

    @Override
    public void setContentState(IRecipesView view, RecipesList recipes) {
        if (view != null) {
            view.setProgressVisible(false);
            view.setErrorVisible(false);
            view.setRecipes(recipes);
            view.setContentVisible(true);
        }
    }

    @Override
    public void setErrorState(IRecipesView view) {
        // ignore
    }

    @Override
    public void release() {
        // ignore
    }
}
