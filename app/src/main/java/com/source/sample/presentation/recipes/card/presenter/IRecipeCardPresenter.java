package com.source.sample.presentation.recipes.card.presenter;
import com.source.sample.presentation.BasePresenter;
import com.source.sample.presentation.recipes.card.view.IRecipeCardView;

/**
 * Created by Roman,
 * Date: 28.10.17,
 * email: gang018@mail.ru
 */

public interface IRecipeCardPresenter extends BasePresenter<IRecipeCardView> {

    void setRecipeId(final String id);

    void controlOpenAuthorButtonClick();

    void controlRetryButtonClick();
}
