package com.source.sample.presentation.recipes.list.view.view;
import android.content.Context;
import android.graphics.Color;
import android.util.TypedValue;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.source.sample.utils.Constants;
import com.source.sample.utils.ValuesConverter;

/**
 * Created by: Roman Zakharov
 * Date: 25.10.2017
 * Email: gang018@mail.ru
 */

public class RecipesListItemView extends RelativeLayout {

    public RecipesListItemView(Context context) {
        super(context);

        init();
    }

    private final void init() {
        final TextView titleTextView = new TextView(getContext());
        titleTextView.setTextColor(Color.BLACK);
        final int padding16 = ValuesConverter.dp2px(ValuesConverter.DP_16);
        titleTextView.setPadding(padding16, padding16, padding16, padding16);
        titleTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX, ValuesConverter.dp2px(ValuesConverter.DP_14));

        final LayoutParams titleParams = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
        titleTextView.setLayoutParams(titleParams);
        addView(titleTextView);
    }

    public final void setTitle(final String title) {
        ((TextView) getChildAt(Constants.ZERO)).setText(title);
    }
}
