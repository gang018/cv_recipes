package com.source.sample.presentation.recipes.list.view.view;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import com.source.sample.data.recipes.model.Recipe;
import com.source.sample.data.recipes.model.RecipesList;
import com.source.sample.utils.Constants;

/**
 * Created by: Roman Zakharov
 * Date: 25.10.2017
 * Email: gang018@mail.ru
 */

public class RecipesListAdapter extends BaseAdapter {

    private RecipesList mRecipesList;

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        final RecipesListItemView itemView;

        if (view != null) {
            itemView = (RecipesListItemView) view;
        } else {
            itemView = new RecipesListItemView(viewGroup.getContext());
        }

        final Recipe recipe = getItem(i);
        itemView.setTitle(recipe.title);
        return itemView;
    }

    @Override
    public Recipe getItem(int i) {
        if (mRecipesList != null) {
            return mRecipesList.get(i);
        }

        return null;
    }

    @Override
    public int getCount() {
        if (mRecipesList != null) {
            return mRecipesList.getCount();
        }

        return Constants.ZERO;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    public final void setRecipesList(final RecipesList list) {
        mRecipesList = list;
        notifyDataSetChanged();
    }

    public final void release() {
        mRecipesList = null;
    }
}
