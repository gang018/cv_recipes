package com.source.sample.presentation.router;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import com.source.sample.R;
import com.source.sample.presentation.recipes.card.view.fragment.RecipeCardFragment;
import com.source.sample.presentation.recipes.list.view.fragment.RecipesListFragment;
import com.source.sample.utils.Constants;
import java.util.List;

/**
 * Created by Roman,
 * Date: 28.10.17,
 * email: gang018@mail.ru
 */

public class AppScreensRouter {

    private static final int DEFAULT_BACKSTACK_SIZE = 1;

    private AppCompatActivity mActivity;
    private FragmentManager.OnBackStackChangedListener mBackstackChangeListener;

    public AppScreensRouter() {
        Log.e("router", "with hashcode = " + hashCode());
    }

    public final void setActivity(final AppCompatActivity activity) {
        mActivity = activity;
        mActivity.getSupportFragmentManager().addOnBackStackChangedListener(getBackstackListener());
    }

    private final FragmentManager.OnBackStackChangedListener getBackstackListener() {
        if (mBackstackChangeListener == null) {
            mBackstackChangeListener = () -> {
                if (mActivity != null && mActivity.getSupportFragmentManager() != null) {
                    final int count = mActivity.getSupportFragmentManager().getBackStackEntryCount();
                    final ActionBar actionBar = mActivity.getSupportActionBar();
                    if (count > DEFAULT_BACKSTACK_SIZE) {
                        actionBar.setHomeAsUpIndicator(R.drawable.ic_arrow_back_white_24dp);
                        actionBar.setDisplayHomeAsUpEnabled(true);
                        actionBar.setDefaultDisplayHomeAsUpEnabled(true);
                    } else {
                        actionBar.setDisplayHomeAsUpEnabled(false);
                        actionBar.setDefaultDisplayHomeAsUpEnabled(false);
                    }
                }
            };
        }

        return mBackstackChangeListener;
    }

    public final void openRecipesList() {
        if (mActivity == null) {
            return;
        }

        final String name = RecipesListFragment.class.getName();
        final Fragment fragment = RecipesListFragment.instantiate(mActivity, name);
        mActivity.getSupportFragmentManager()
            .beginTransaction()
            .addToBackStack(name)
            .add(R.id.main_activity_fragment_container, fragment, name)
            .commit();
    }

    public final void openRecipeCard(final String recipeId) {
        if (mActivity == null) {
            return;
        }

        final String name = RecipeCardFragment.class.getName();
        final RecipeCardFragment fragment = (RecipeCardFragment) RecipeCardFragment.instantiate(mActivity, name);
        fragment.setRecipeId(recipeId);
        mActivity.getSupportFragmentManager()
                .beginTransaction()
                .addToBackStack(name)
                .add(R.id.main_activity_fragment_container, fragment, name)
                .commit();
    }

    public final void openAuthorsPage(final String authorsPageUrl) {
        if (authorsPageUrl != null && !authorsPageUrl.isEmpty() && mActivity != null) {
            final Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse(authorsPageUrl));
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            mActivity.startActivity(intent);
        }
    }

    public final boolean handleBackPressed() {
        final Fragment currentFragment = getCurrentFragment();
        if (currentFragment instanceof RecipeCardFragment) {
            popBackStack();
            return true;
        } else if (currentFragment instanceof RecipesListFragment) {
            popBackStack();
            return false;
        }

        return false;
    }

    private final void popBackStack() {
        if (mActivity != null && mActivity.getSupportFragmentManager() != null) {
            mActivity.getSupportFragmentManager().popBackStack();
        }
    }

    private final Fragment getCurrentFragment() {
        if (mActivity != null && mActivity.getSupportFragmentManager() != null) {
            final FragmentManager manager = mActivity.getSupportFragmentManager();
            final int fragmentsCount = manager.getBackStackEntryCount();
            if (fragmentsCount > Constants.ZERO) {
                final List<Fragment> fragments = manager.getFragments();
                return fragments.get(fragmentsCount - 1);
            }
        }

        return null;
    }

    public final void release() {
        if (mActivity != null) {
            mActivity.getSupportFragmentManager().removeOnBackStackChangedListener(getBackstackListener());
        }

        mBackstackChangeListener = null;
        mActivity = null;
    }
}
