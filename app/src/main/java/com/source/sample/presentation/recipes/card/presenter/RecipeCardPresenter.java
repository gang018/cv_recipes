package com.source.sample.presentation.recipes.card.presenter;
import com.source.sample.data.recipes.model.Recipe;
import com.source.sample.domain.recipes.IRecipesInteractor;
import io.reactivex.android.schedulers.AndroidSchedulers;
import com.source.sample.presentation.recipes.card.view.IRecipeCardView;
import com.source.sample.presentation.recipes.card.view.view.states.IRecipeCardViewState;
import com.source.sample.presentation.recipes.card.view.view.states.RecipeCardViewStateMachine;

/**
 * Created by: Roman Zakharov
 * Date: 03.11.2017
 * Email: gang018@mail.ru
 */

public class RecipeCardPresenter implements IRecipeCardPresenter {

    private IRecipeCardView mRecipeCardView;
    private IRecipeCardViewState mStateMachine;

    private IRecipesInteractor mInteractor;

    private String mRecipeId;
    private Recipe mCurrentRecipe;

    public RecipeCardPresenter(final IRecipesInteractor interactor) {
        mInteractor = interactor;
    }

    @Override
    public void bindView(IRecipeCardView view) {
        mRecipeCardView = view;
        mStateMachine = new RecipeCardViewStateMachine();
        mStateMachine.setLoadingState(view);
    }

    @Override
    public void setRecipeId(String id) {
        mRecipeId = id;
        loadRecipe(mRecipeId);
    }

    private final void loadRecipe(final String recipeId) {
        if (mStateMachine != null) {
            mStateMachine.setLoadingState(mRecipeCardView);
        }

        if (mInteractor != null) {
            mInteractor.loadRecipe(recipeId)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                            result -> handleResult(result),
                            error -> handleResult(null)
                    );
        }
    }

    private final void handleResult(final Recipe recipe) {
        mCurrentRecipe = recipe;
        if (recipe != null) {
            if (mStateMachine != null) {
                mStateMachine.setContentState(mRecipeCardView, recipe);
            }
        } else {
            if (mStateMachine != null) {
                mStateMachine.setErrorState(mRecipeCardView);
            }
        }
    }

    @Override
    public void controlOpenAuthorButtonClick() {
        if (mRecipeCardView != null && mCurrentRecipe != null) {
            mRecipeCardView.openAuthorsPage(mCurrentRecipe.publisher_url);
        }
    }

    @Override
    public void controlRetryButtonClick() {
        loadRecipe(mRecipeId);
    }

    @Override
    public void release() {
        mRecipeCardView = null;

        if (mStateMachine != null) {
            mStateMachine.release();
        }
        mStateMachine = null;
    }
}
