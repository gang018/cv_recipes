package com.source.sample.presentation.recipes.card.view.fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.source.sample.data.recipes.model.Recipe;
import com.source.sample.di.DiManager;
import com.source.sample.presentation.recipes.card.presenter.IRecipeCardPresenter;
import com.source.sample.presentation.recipes.card.view.IRecipeCardView;
import com.source.sample.presentation.recipes.card.view.view.RecipeCardView;
import com.source.sample.presentation.router.AppScreensRouter;
import javax.inject.Inject;

/**
 * Created by Roman,
 * Date: 28.10.17,
 * email: gang018@mail.ru
 */

public class RecipeCardFragment extends Fragment implements IRecipeCardView {

    @Inject
    AppScreensRouter mRouter;

    @Inject
    IRecipeCardPresenter mPresenter;

    private String mRecipeId;
    private RecipeCardView mView;

    public RecipeCardFragment() {
        DiManager.getRecipesComponent().inject(this);
    }

    public final void setRecipeId(final String id) {
        mRecipeId = id;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        mView = new RecipeCardView(inflater.getContext());
        mView.setOnOpenAuthorButtonClickListener(mOpenAuthorClickListener);
        mView.setOnRetryButtonClickListener(mRetryClickListener);

        mPresenter.bindView(this);
        mPresenter.setRecipeId(mRecipeId);
        return mView;
    }

    @Override
    public void setErrorVisible(boolean visible) {
        if (mView != null) {
            mView.setErrorViewVisibility(visible? View.VISIBLE: View.INVISIBLE);
        }
    }

    @Override
    public void setContentVisible(boolean visible) {
        if (mView != null) {
            mView.setContentVisibility(visible? View.VISIBLE: View.INVISIBLE);
        }
    }

    @Override
    public void setLoaderVisible(boolean visible) {
        if (mView != null) {
            mView.setLoaderVisibility(visible? View.VISIBLE: View.INVISIBLE);
        }
    }

    @Override
    public void setRecipeData(Recipe recipe) {
        if (mView != null) {
            mView.setRecipeImage(recipe.image_url);
            mView.setRecipeTitle(recipe.title);
            mView.setAuthorTitle(recipe.publisher);
        }
    }

    @Override
    public void openAuthorsPage(String authorPageUrl) {
        if (mRouter != null) {
            mRouter.openAuthorsPage(authorPageUrl);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        mView = null;
        mOpenAuthorClickListener = null;

        if (mPresenter != null) {
            mPresenter.release();
        }
        mPresenter = null;
    }

    private View.OnClickListener mOpenAuthorClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (mPresenter != null) {
                mPresenter.controlOpenAuthorButtonClick();
            }
        }
    };

    private View.OnClickListener mRetryClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (mPresenter != null) {
                mPresenter.controlRetryButtonClick();
            }
        }
    };
}
