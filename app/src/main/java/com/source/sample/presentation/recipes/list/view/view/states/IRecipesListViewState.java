package com.source.sample.presentation.recipes.list.view.view.states;
import com.source.sample.data.recipes.model.RecipesList;
import com.source.sample.presentation.recipes.list.view.IRecipesView;

/**
 * Created by Roman,
 * Date: 05.11.17,
 * email: gang018@mail.ru
 */

public interface IRecipesListViewState {

    void setLoadingState(final IRecipesView view);

    void setContentState(final IRecipesView view, final RecipesList recipes);

    void setErrorState(final IRecipesView view);

    void release();
}
