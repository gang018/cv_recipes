package com.source.sample.presentation.mainscreen.presenter;
import com.source.sample.presentation.mainscreen.view.IMainScreenView;

/**
 * Created by: Roman Zakharov
 * Date: 24.10.2017
 * Email: gang018@mail.ru
 */

public class MainScreenPresenter implements IMainScreenPresenter {

    private IMainScreenView mView;

    public MainScreenPresenter() {
    }

    @Override
    public void bindView(IMainScreenView view) {
        mView = view;

        if (mView != null) {
            mView.showRecipesList();
        }
    }

    @Override
    public void release() {
        mView = null;
    }
}
