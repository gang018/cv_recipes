package com.source.sample.presentation.recipes.list.view.fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.source.sample.data.recipes.model.Recipe;
import com.source.sample.data.recipes.model.RecipesList;
import com.source.sample.di.DiManager;
import com.source.sample.presentation.recipes.list.presenter.IRecipesPresenter;
import com.source.sample.presentation.recipes.list.view.IRecipesView;
import com.source.sample.presentation.recipes.list.view.view.RecipesView;
import com.source.sample.presentation.router.AppScreensRouter;
import javax.inject.Inject;

/**
 * Created by: Roman Zakharov
 * Date: 24.10.2017
 * Email: gang018@mail.ru
 */

public class RecipesListFragment extends Fragment implements IRecipesView {

    @Inject
    IRecipesPresenter mPresenter;

    @Inject
    AppScreensRouter mRouter;

    private RecipesView mView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        DiManager.getRecipesComponent().inject(this);

        mView = new RecipesView(inflater.getContext());
        mView.setOnRecipeClickListener(mRecipeClickListener);
        mView.setOnRetryClickListener(mRetryClickListener);
        mPresenter.bindView(this);
        return mView;
    }

    @Override
    public void setProgressVisible(boolean visible) {
        if (mView != null) {
            mView.setProgressVisible(visible? View.VISIBLE: View.INVISIBLE);
        }
    }

    @Override
    public void setContentVisible(boolean visible) {
        if (mView != null) {
            mView.setContentVisible(visible? View.VISIBLE: View.INVISIBLE);
        }
    }

    @Override
    public void setErrorVisible(boolean visible) {
        if (mView != null) {
            mView.setErrorVisible(visible? View.VISIBLE: View.INVISIBLE);
        }
    }

    @Override
    public void setRecipes(RecipesList recipes) {
        if (mView != null) {
            mView.setRecipes(recipes);
        }
    }

    @Override
    public void showRecipeCard(Recipe recipe) {
        if (mRouter != null) {
            mRouter.openRecipeCard(recipe.recipe_id);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        mView = null;
        mRecipeClickListener = null;
        mRetryClickListener = null;
        mRouter = null;
    }

    private View.OnClickListener mRetryClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (mPresenter != null) {
                mPresenter.controlRetryButtonClick();
            }
        }
    };

    private RecipeClickListener mRecipeClickListener = new RecipeClickListener() {
        @Override
        public void onRecipeClick(Recipe recipe) {
            if (mPresenter != null) {
                mPresenter.controlRecipeItemClick(recipe);
            }
        }
    };

    public interface RecipeClickListener {
        void onRecipeClick(final Recipe recipe);
    }
}
