package com.source.sample.presentation.mainscreen.view;

/**
 * Created by: Roman Zakharov
 * Date: 24.10.2017
 * Email: gang018@mail.ru
 */

public interface IMainScreenView {

    void showRecipesList();
}
