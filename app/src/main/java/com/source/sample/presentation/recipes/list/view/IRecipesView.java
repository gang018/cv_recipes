package com.source.sample.presentation.recipes.list.view;
import com.source.sample.data.recipes.model.Recipe;
import com.source.sample.data.recipes.model.RecipesList;

/**
 * Created by Roman,
 * Date: 22.10.17,
 * email: gang018@mail.ru
 */

public interface IRecipesView {

    void setProgressVisible(final boolean visible);

    void setErrorVisible(final boolean visible);

    void setContentVisible(final boolean visible);

    void setRecipes(final RecipesList recipes);

    void showRecipeCard(final Recipe recipe);
}
