package com.source.sample.presentation.recipes.card.view;
import com.source.sample.data.recipes.model.Recipe;

/**
 * Created by Roman,
 * Date: 28.10.17,
 * email: gang018@mail.ru
 */

public interface IRecipeCardView {

    void setRecipeData(final Recipe recipe);

    void setContentVisible(final boolean visible);

    void setErrorVisible(final boolean visible);

    void setLoaderVisible(final boolean visible);

    void openAuthorsPage(final String authorPageUrl);
}
