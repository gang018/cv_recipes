package com.source.sample.presentation.mainscreen.presenter;
import com.source.sample.presentation.BasePresenter;
import com.source.sample.presentation.mainscreen.view.IMainScreenView;

/**
 * Created by: Roman Zakharov
 * Date: 24.10.2017
 * Email: gang018@mail.ru
 */

public interface IMainScreenPresenter extends BasePresenter<IMainScreenView> {
}
