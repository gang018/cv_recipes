package com.source.sample.presentation.recipes.list.view.view.states;
import com.source.sample.data.recipes.model.RecipesList;
import com.source.sample.presentation.recipes.list.view.IRecipesView;

/**
 * Created by Roman,
 * Date: 05.11.17,
 * email: gang018@mail.ru
 */

public class RecipesListViewStateMachine implements IRecipesListViewState {

    private IRecipesListViewState mLoadingState;
    private IRecipesListViewState mContentState;
    private IRecipesListViewState mErrorState;

    private IRecipesListViewState mCurrentState;

    public RecipesListViewStateMachine() {
        mLoadingState = new LoadingState();
        mContentState = new ContentState();
        mErrorState = new ErrorState();
    }

    @Override
    public void setLoadingState(IRecipesView view) {
        if (mCurrentState != mLoadingState) {
            mCurrentState = mLoadingState;
            mCurrentState.setLoadingState(view);
        }
    }

    @Override
    public void setContentState(IRecipesView view, RecipesList recipe) {
        if (mCurrentState != mContentState) {
            mCurrentState = mContentState;
            mCurrentState.setContentState(view, recipe);
        }
    }

    @Override
    public void setErrorState(IRecipesView view) {
        if (mCurrentState != mErrorState) {
            mCurrentState = mErrorState;
            mCurrentState.setErrorState(view);
        }
    }

    @Override
    public void release() {
        mLoadingState = null;
        mErrorState = null;
        mContentState = null;
        mCurrentState = null;
    }
}
