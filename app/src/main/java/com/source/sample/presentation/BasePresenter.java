package com.source.sample.presentation;

/**
 * Created by: Roman Zakharov
 * Date: 16.10.2017
 * Email: gang018@mail.ru
 */

public interface BasePresenter<T> {

    void bindView(T view);

    void release();
}
