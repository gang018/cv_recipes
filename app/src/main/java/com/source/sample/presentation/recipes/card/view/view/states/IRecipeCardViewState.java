package com.source.sample.presentation.recipes.card.view.view.states;
import com.source.sample.data.recipes.model.Recipe;
import com.source.sample.presentation.recipes.card.view.IRecipeCardView;

/**
 * Created by Roman,
 * Date: 05.11.17,
 * email: gang018@mail.ru
 */

public interface IRecipeCardViewState {

    void setLoadingState(final IRecipeCardView view);

    void setContentState(final IRecipeCardView view, final Recipe recipe);

    void setErrorState(final IRecipeCardView view);

    void release();
}
