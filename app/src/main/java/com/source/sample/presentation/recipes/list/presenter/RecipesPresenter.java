package com.source.sample.presentation.recipes.list.presenter;
import android.util.Log;
import com.source.sample.data.recipes.model.Recipe;
import com.source.sample.data.recipes.model.RecipesList;
import com.source.sample.domain.recipes.IRecipesInteractor;
import io.reactivex.android.schedulers.AndroidSchedulers;
import com.source.sample.presentation.recipes.list.view.IRecipesView;
import com.source.sample.presentation.recipes.list.view.view.states.IRecipesListViewState;
import com.source.sample.presentation.recipes.list.view.view.states.RecipesListViewStateMachine;
import com.source.sample.utils.Constants;

/**
 * Created by Roman,
 * Date: 22.10.17,
 * email: gang018@mail.ru
 */

public class RecipesPresenter implements IRecipesPresenter {

    private IRecipesListViewState mViewStateMachine;
    private IRecipesView mView;
    private IRecipesInteractor mInteractor;

    public RecipesPresenter(final IRecipesInteractor interactor) {
        mInteractor = interactor;
    }

    @Override
    public void bindView(IRecipesView view) {
        mView = view;
        mViewStateMachine = new RecipesListViewStateMachine();

        loadAndShowRecipes();
    }

    private final void loadAndShowRecipes() {
        if (mViewStateMachine != null) {
            mViewStateMachine.setLoadingState(mView);
        }

        if (mInteractor != null) {
            mInteractor.loadRecipes()
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                            result -> showLoadingResult(result),
                            error -> showLoadingResult(null)
                    );
        }
    }

    private final void showLoadingResult(final RecipesList recipes) {
        Log.e("TAG", "observer result = " + recipes + ", " + (recipes == null? Constants.ZERO: recipes.mRecipes));
        if (recipes != null) {
            if (mViewStateMachine != null) {
                mViewStateMachine.setContentState(mView, recipes);
            }
        } else {
            if (mViewStateMachine != null) {
                mViewStateMachine.setErrorState(mView);
            }
        }
    }

    @Override
    public void controlRecipeItemClick(final Recipe recipe) {
        if (mView != null) {
            mView.showRecipeCard(recipe);
        }
    }

    @Override
    public void controlRetryButtonClick() {
        loadAndShowRecipes();
    }

    @Override
    public void release() {
        mView = null;

        if (mInteractor != null) {
            mInteractor.release();
        }
        mInteractor = null;
    }
}
