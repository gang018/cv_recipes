package com.source.sample.presentation.recipes.card.view.view.states;
import com.source.sample.data.recipes.model.Recipe;
import com.source.sample.presentation.recipes.card.view.IRecipeCardView;

/**
 * Created by Roman,
 * Date: 05.11.17,
 * email: gang018@mail.ru
 */

public class RecipeCardViewStateMachine implements IRecipeCardViewState {

    private IRecipeCardViewState mLoadingState;
    private IRecipeCardViewState mContentState;
    private IRecipeCardViewState mErrorState;

    private IRecipeCardViewState mCurrentState;

    public RecipeCardViewStateMachine() {
        mLoadingState = new LoadingState();
        mContentState = new ContentState();
        mErrorState = new ErrorState();
    }

    @Override
    public void setLoadingState(IRecipeCardView view) {
        if (mCurrentState != mLoadingState) {
            mCurrentState = mLoadingState;
            mCurrentState.setLoadingState(view);
        }
    }

    @Override
    public void setContentState(IRecipeCardView view, Recipe recipe) {
        if (mCurrentState != mContentState) {
            mCurrentState = mContentState;
            mCurrentState.setContentState(view, recipe);
        }
    }

    @Override
    public void setErrorState(IRecipeCardView view) {
        if (mCurrentState != mErrorState) {
            mCurrentState = mErrorState;
            mCurrentState.setErrorState(view);
        }
    }

    @Override
    public void release() {
        mLoadingState = null;
        mErrorState = null;
        mContentState = null;
        mCurrentState = null;
    }
}
