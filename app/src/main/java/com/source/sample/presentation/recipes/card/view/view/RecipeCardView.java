package com.source.sample.presentation.recipes.card.view.view;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatButton;
import android.util.TypedValue;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.bumptech.glide.GenericTransitionOptions;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.transition.ViewPropertyTransition;
import com.source.sample.R;
import com.source.sample.presentation.widgets.ErrorView;
import com.source.sample.utils.Constants;
import com.source.sample.utils.ValuesConverter;
import com.source.sample.utils.glide.GlideApp;

/**
 * Created by Roman,
 * Date: 28.10.17,
 * email: gang018@mail.ru
 */

public class RecipeCardView extends RelativeLayout {

    private static final float IMAGE_HEIGHT_MULT = 0.3f;
    private static final float ALPHA_ZERO = 0f;
    private static final float ALPHA_FULL = 1f;

    private static final int IMAGE_ANIMATION_DURATION = 400;

    private ImageView mRecipeImageView;
    private TextView mRecipeTitleView;
    private TextView mAuthorView;
    private ProgressBar mProgressBar;
    private AppCompatButton mOpenAuthorButton;
    private ErrorView mErrorView;

    public RecipeCardView(Context context) {
        super(context);

        init();
    }

    private final void init() {
        setBackgroundColor(Color.WHITE);
        setClickable(true);

        // Loader
        mProgressBar = new ProgressBar(getContext());
        mProgressBar.setIndeterminate(true);

        final LayoutParams progressParams = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        progressParams.addRule(CENTER_IN_PARENT);
        mProgressBar.setLayoutParams(progressParams);
        addView(mProgressBar);


        // Error view
        mErrorView = new ErrorView(getContext());
        mErrorView.setVisibility(INVISIBLE);

        final LayoutParams errorParams = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
        errorParams.addRule(CENTER_IN_PARENT);
        mErrorView.setLayoutParams(errorParams);
        addView(mErrorView);


        // Recipe image
        mRecipeImageView = new ImageView(getContext());
        mRecipeImageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        mRecipeImageView.setId(android.R.id.icon);

        final int height = (int) (getResources().getDisplayMetrics().heightPixels * IMAGE_HEIGHT_MULT);
        final LayoutParams imageViewParams = new LayoutParams(LayoutParams.MATCH_PARENT, height);
        mRecipeImageView.setLayoutParams(imageViewParams);
        addView(mRecipeImageView);


        // Recipe title
        mRecipeTitleView = new TextView(getContext());
        mRecipeTitleView.setTextColor(Color.DKGRAY);
        mRecipeTitleView.setTextSize(TypedValue.COMPLEX_UNIT_PX, ValuesConverter.dp2px(ValuesConverter.DP_18));
        mRecipeTitleView.setId(android.R.id.text1);
        final int titlePadding = ValuesConverter.dp2px(ValuesConverter.DP_8);
        mRecipeTitleView.setPadding(titlePadding, titlePadding, titlePadding, titlePadding);

        final LayoutParams titlePArams = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
        titlePArams.addRule(BELOW, android.R.id.icon);
        mRecipeTitleView.setLayoutParams(titlePArams);
        addView(mRecipeTitleView);


        // Author
        mAuthorView = new TextView(getContext());
        mAuthorView.setTextColor(Color.GRAY);
        mAuthorView.setTextSize(TypedValue.COMPLEX_UNIT_PX, ValuesConverter.dp2px(ValuesConverter.DP_14));
        mAuthorView.setTypeface(null, Typeface.ITALIC);
        mAuthorView.setPadding(titlePadding, Constants.ZERO, titlePadding, Constants.ZERO);

        final LayoutParams authorParams = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
        authorParams.addRule(BELOW, android.R.id.text1);
        mAuthorView.setLayoutParams(authorParams);
        addView(mAuthorView);


        // Open Author button
        mOpenAuthorButton = new AppCompatButton(getContext());
        mOpenAuthorButton.setText(R.string.recipe_card_button_open_author);
        mOpenAuthorButton.setTextColor(Color.WHITE);
        mOpenAuthorButton.setSupportBackgroundTintList(ColorStateList.valueOf(
                ContextCompat.getColor(getContext(), R.color.colorAccent)
        ));

        final LayoutParams buttonParams = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
        buttonParams.addRule(ALIGN_PARENT_BOTTOM);
        buttonParams.setMargins(titlePadding, titlePadding, titlePadding, titlePadding);
        mOpenAuthorButton.setLayoutParams(buttonParams);
        addView(mOpenAuthorButton);
    }

    public final void setRecipeTitle(final String title) {
        if (mRecipeTitleView != null) {
            mRecipeTitleView.setText(title);
        }
    }

    public final void setAuthorTitle(final String title) {
        if (mAuthorView != null) {
            mAuthorView.setText(title);
        }
    }

    public final void setRecipeImage(final String imagePath) {
        if (mRecipeImageView != null) {

            GlideApp.with(getContext())
                    .load(imagePath)
                    .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                    .transition(GenericTransitionOptions.with(mImageAnimator))
                    .centerCrop()
                    .into(mRecipeImageView);
        }
    }

    public final void setLoaderVisibility(final int visibility) {
        if (mProgressBar != null) {
            mProgressBar.setVisibility(visibility);
        }
    }

    public final void setContentVisibility(final int visibility) {
        if (mAuthorView != null) {
            mAuthorView.setVisibility(visibility);
        }

        if (mOpenAuthorButton != null) {
            mOpenAuthorButton.setVisibility(visibility);
        }

        if (mRecipeImageView != null) {
            mRecipeImageView.setVisibility(visibility);
        }

        if (mRecipeTitleView != null) {
            mRecipeTitleView.setVisibility(visibility);
        }
    }

    public final void setErrorViewVisibility(final int visibility) {
        if (mErrorView != null) {
            mErrorView.setVisibility(visibility);
        }
    }

    public final void setOnOpenAuthorButtonClickListener(final OnClickListener listener) {
        if (mOpenAuthorButton != null) {
            mOpenAuthorButton.setOnClickListener(listener);
        }
    }

    public final void setOnRetryButtonClickListener(final OnClickListener listener) {
        if (mErrorView != null) {
            mErrorView.setOnRetryClickListener(listener);
        }
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();

        mRecipeTitleView = null;
        mRecipeImageView = null;
        mImageAnimator = null;
        mOpenAuthorButton = null;
        mAuthorView = null;
        mProgressBar = null;
        mErrorView = null;
    }

    private ViewPropertyTransition.Animator mImageAnimator = new ViewPropertyTransition.Animator() {
        @Override
        public void animate(View view) {
            view.setAlpha(ALPHA_ZERO);

            ObjectAnimator.ofFloat(view, ALPHA, ALPHA_ZERO, ALPHA_FULL)
                    .setDuration(IMAGE_ANIMATION_DURATION)
                    .start();
        }
    };
}
