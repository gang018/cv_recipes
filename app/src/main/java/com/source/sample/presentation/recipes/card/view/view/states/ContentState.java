package com.source.sample.presentation.recipes.card.view.view.states;
import com.source.sample.data.recipes.model.Recipe;
import com.source.sample.presentation.recipes.card.view.IRecipeCardView;

/**
 * Created by Roman,
 * Date: 05.11.17,
 * email: gang018@mail.ru
 */

class ContentState implements IRecipeCardViewState {
    @Override
    public void setLoadingState(IRecipeCardView view) {
        // ignore
    }

    @Override
    public void setContentState(IRecipeCardView view, Recipe recipe) {
        if (view != null) {
            view.setLoaderVisible(false);
            view.setErrorVisible(false);
            view.setRecipeData(recipe);
            view.setContentVisible(true);
        }
    }

    @Override
    public void setErrorState(IRecipeCardView view) {
        // ignore
    }

    @Override
    public void release() {
        // ignore
    }
}
