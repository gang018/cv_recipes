package com.source.sample.presentation.widgets;
import android.content.Context;
import android.graphics.Color;
import android.util.TypedValue;
import android.view.Gravity;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.source.sample.R;
import com.source.sample.utils.ValuesConverter;

/**
 * Created by Roman,
 * Date: 06.11.17,
 * email: gang018@mail.ru
 */
public class ErrorView extends RelativeLayout {

    private ImageView mIcon;
    private TextView mTextView;

    public ErrorView(Context context) {
        super(context);
        init();
    }

    private final void init() {
        setGravity(Gravity.CENTER);


        // Error icon
        mIcon = new ImageView(getContext());
        mIcon.setId(android.R.id.icon);
        mIcon.setImageResource(R.drawable.ic_sentiment_very_dissatisfied_black_36dp);

        final LayoutParams iconParams = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        iconParams.addRule(CENTER_HORIZONTAL);
        mIcon.setLayoutParams(iconParams);
        addView(mIcon);


        // Error title
        mTextView = new TextView(getContext());
        mTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX, ValuesConverter.dp2px(ValuesConverter.DP_14));
        mTextView.setText(R.string.error_view_title);
        mTextView.setTextColor(Color.DKGRAY);
        mTextView.setGravity(Gravity.CENTER_HORIZONTAL);

        final LayoutParams textParams = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
        textParams.addRule(BELOW, android.R.id.icon);
        final int margin = ValuesConverter.dp2px(ValuesConverter.DP_14);
        textParams.setMargins(margin, margin, margin, margin);
        mTextView.setLayoutParams(textParams);
        addView(mTextView);
    }

    public final void setOnRetryClickListener(final OnClickListener listener) {
        if (mTextView != null) {
            mTextView.setOnClickListener(listener);
        }
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        mIcon = null;
        mTextView = null;
    }
}