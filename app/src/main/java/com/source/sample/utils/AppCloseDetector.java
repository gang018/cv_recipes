package com.source.sample.utils;
import android.app.Activity;
import android.app.Application;
import android.os.Bundle;
import android.util.Log;
import com.source.sample.RecipesApplication;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Roman,
 * Date: 05.11.17,
 * email: gang018@mail.ru
 */

public class AppCloseDetector {

    private static final String TAG = AppCloseDetector.class.getSimpleName();

    private List<String> mRunningActivities;

    public AppCloseDetector() {
        mRunningActivities = Collections.synchronizedList(new ArrayList<String>());
        RecipesApplication.getInstance().registerActivityLifecycleCallbacks(mCallbacks);
    }

    public final void release() {
        RecipesApplication.getInstance().unregisterActivityLifecycleCallbacks(mCallbacks);
        mCallbacks = null;
    }

    private Application.ActivityLifecycleCallbacks mCallbacks = new Application.ActivityLifecycleCallbacks() {
        @Override
        public void onActivityCreated(Activity activity, Bundle bundle) {
            trackActivityCreate(activity.getClass().getName());
        }

        @Override
        public void onActivityStarted(Activity activity) {
            // ignore
        }

        @Override
        public void onActivityResumed(Activity activity) {
            // ignore
        }

        @Override
        public void onActivityPaused(Activity activity) {
            // ignore
        }

        @Override
        public void onActivityStopped(Activity activity) {
            // ignore
        }

        @Override
        public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
            // ignore
        }

        @Override
        public void onActivityDestroyed(Activity activity) {
            trackActivityDestroy(activity.getClass().getName());
            tryCloseApp();
        }
    };

    private final void trackActivityCreate(final String activityName) {
        if (mRunningActivities != null) {
            mRunningActivities.add(activityName);
            logList();
        }
    }

    private final void logList() {
        if (mRunningActivities != null) {
            Log.e(TAG, "logging list of activities .....");
            if (mRunningActivities.size() == Constants.ZERO) {
                Log.e(TAG, "no running activities was found");
                return;
            }

            for (String name: mRunningActivities) {
                Log.e(TAG,name + " is running");
            }
        }
    }

    private final void trackActivityDestroy(final String activityName) {
        if (mRunningActivities != null) {
            mRunningActivities.remove(activityName);
            logList();
        }
    }

    private final void tryCloseApp() {
        if (!isAppRunning()) {
            RecipesApplication.getInstance().releaseResources();
        }
    }

    private final boolean isAppRunning() {
        return mRunningActivities != null && !mRunningActivities.isEmpty();
    }
}
