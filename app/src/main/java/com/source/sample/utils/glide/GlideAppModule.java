package com.source.sample.utils.glide;
import com.bumptech.glide.annotation.GlideModule;
import com.bumptech.glide.module.AppGlideModule;

/**
 * Created by Roman,
 * Date: 05.11.17,
 * email: gang018@mail.ru
 */

@GlideModule
public final class GlideAppModule extends AppGlideModule {
}
