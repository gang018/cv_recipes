package com.source.sample.utils;
import android.content.res.Resources;
import java.util.HashMap;

/**
 * Created by: Roman Zakharov
 * Date: 25.10.2017
 * Email: gang018@mail.ru
 */

public class ValuesConverter
{
    public static final int DP_1 = 1,
            DP_2 = 2,
            DP_3 = 3,
            DP_4 = 4,
            DP_5 = 5,
            DP_8 = 8,
            DP_10 = 10,
            DP_12 = 12,
            DP_14 = 14,
            DP_15 = 15,
            DP_16 = 16,
            DP_18 = 18,
            DP_20 = 20,
            DP_25 = 25,
            DP_22 = 22,
            DP_32 = 32,
            DP_35 = 35,
            DP_50 = 50,
            DP_60 = 60,
            DP_80 = 80;


    private static final HashMap<Integer, Integer> CONVERTED_VALUES = new HashMap<>();

    public static final int dp2px(final int dp) {
        Integer value = CONVERTED_VALUES.get(dp);
        if (value == null) {
            value = (int) (dp * Resources.getSystem().getDisplayMetrics().density);
            CONVERTED_VALUES.put(dp, value);
        }
        return value;
    }

    public static int pxToDp(final int px) {
        return (int) (px / Resources.getSystem().getDisplayMetrics().density);
    }
}