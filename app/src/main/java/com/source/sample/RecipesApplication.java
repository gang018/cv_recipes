package com.source.sample;
import android.app.Application;
import com.source.sample.data.utils.remote.ApiManager;
import com.source.sample.utils.AppCloseDetector;

/**
 * Created by: Roman Zakharov
 * Date: 16.10.2017
 * Email: gang018@mail.ru
 */

public class RecipesApplication extends Application {

    private static RecipesApplication sInstance;

    public static final RecipesApplication getInstance() {
        return sInstance;
    }

    private ApiManager mApiManager;
    private AppCloseDetector mDetector;

    @Override
    public void onCreate() {
        super.onCreate();

        sInstance = this;
        mDetector = new AppCloseDetector();
        mApiManager = getApiManager();
    }

    public final ApiManager getApiManager() {
        if (mApiManager == null) {
            mApiManager = new ApiManager();
        }

        return mApiManager;
    }

    public final void releaseResources() {
        if (mApiManager != null) {
            mApiManager.release();
        }
        mApiManager = null;

        if (mDetector != null) {
            mDetector.release();
        }
        mDetector = null;
    }
}
