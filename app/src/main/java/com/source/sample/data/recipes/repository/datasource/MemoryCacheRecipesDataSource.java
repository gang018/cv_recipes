package com.source.sample.data.recipes.repository.datasource;
import com.source.sample.data.recipes.model.Recipe;
import com.source.sample.data.recipes.model.RecipesList;
import io.reactivex.Observable;

/**
 * Created by Roman,
 * Date: 21.10.17,
 * email: gang018@mail.ru
 */

class MemoryCacheRecipesDataSource implements IRecipesDataSource {

    private RecipesList mRecipesList;

    @Override
    public Observable<RecipesList> loadRecipes() {
        return Observable.just(new RecipesList(mRecipesList));
    }

    @Override
    public void saveRecipes(RecipesList list) {
        mRecipesList = list;
    }

    @Override
    public Observable<Recipe> getRecipe(final String id) {
        return Observable.fromCallable(() -> {
            if (mRecipesList != null) {
                return mRecipesList.getRecipe(id);
            }

            return null;
        });
    }

    @Override
    public void release() {
        mRecipesList = null;
    }
}
