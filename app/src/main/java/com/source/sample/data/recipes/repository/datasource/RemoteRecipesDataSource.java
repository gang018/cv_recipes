package com.source.sample.data.recipes.repository.datasource;
import com.source.sample.RecipesApplication;
import com.source.sample.data.recipes.model.Recipe;
import com.source.sample.data.recipes.model.RecipesList;
import com.source.sample.data.utils.remote.ApiManager;
import io.reactivex.Observable;

/**
 * Created by Roman,
 * Date: 21.10.17,
 * email: gang018@mail.ru
 */

class RemoteRecipesDataSource implements IRecipesDataSource {

    @Override
    public Observable<RecipesList> loadRecipes() {
        final ApiManager manager = RecipesApplication.getInstance().getApiManager();
        return manager.getRecipesApi().queryRecipes();
    }

    @Override
    public void saveRecipes(RecipesList list) {
        // ignore
    }

    @Override
    public Observable<Recipe> getRecipe(String id) {
        // ignore
        return null;
    }

    @Override
    public void release() {

    }
}
