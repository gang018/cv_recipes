package com.source.sample.data.recipes.repository.datasource;

/**
 * Created by Roman,
 * Date: 21.10.17,
 * email: gang018@mail.ru
 */

public class RecipesDataSourceFactory {

    public static final IRecipesDataSource createMemoryCacheDataSource() {
        return new MemoryCacheRecipesDataSource();
    }

    public static final IRecipesDataSource createRemoteDataSource() {
        return new RemoteRecipesDataSource();
    }
}
