package com.source.sample.data.recipes.model;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by: Roman Zakharov
 * Date: 16.10.2017
 * Email: gang018@mail.ru
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class RecipesList {

    @JsonProperty("count")
    public long mCount;

    @JsonProperty("recipes")
    public List<Recipe> mRecipes;

    public RecipesList() {

    }

    public RecipesList(final RecipesList list) {
        if (list != null) {
            mCount = list.mCount;
            mRecipes = new ArrayList<>();

            if (list.mRecipes != null) {
                for (Recipe recipe: list.mRecipes) {
                    mRecipes.add(recipe);
                }
            }
        }
    }

    public final Recipe getRecipe(final String id) {
        if (mRecipes == null) {
            return null;
        }

        for (Recipe recipe: mRecipes) {
            if (recipe.recipe_id.equals(id)) {
                return recipe;
            }
        }

        return null;
    }

    public final Recipe get(final int index) {
        if (mRecipes == null || mRecipes.size() < index) {
            return null;
        }

        return mRecipes.get(index);
    }

    public final int getCount() {
        return (int) mCount;
    }

    public final boolean isEmpty() {
        return mRecipes == null || mRecipes.isEmpty();
    }
}
