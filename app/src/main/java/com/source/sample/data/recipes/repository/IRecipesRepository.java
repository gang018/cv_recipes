package com.source.sample.data.recipes.repository;
import com.source.sample.data.BaseRepository;
import com.source.sample.data.recipes.model.Recipe;
import com.source.sample.data.recipes.model.RecipesList;
import io.reactivex.Observable;

/**
 * Created by Roman,
 * Date: 21.10.17,
 * email: gang018@mail.ru
 */

public interface IRecipesRepository extends BaseRepository {

    Observable<RecipesList> loadRecipes();

    Observable<Recipe> getRecipe(final String id);
}
