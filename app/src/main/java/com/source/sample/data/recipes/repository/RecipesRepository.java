package com.source.sample.data.recipes.repository;
import android.util.Log;
import com.source.sample.data.recipes.model.Recipe;
import com.source.sample.data.recipes.model.RecipesList;
import com.source.sample.data.recipes.repository.datasource.IRecipesDataSource;
import com.source.sample.data.recipes.repository.datasource.RecipesDataSourceFactory;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Roman,
 * Date: 21.10.17,
 * email: gang018@mail.ru
 */

public class RecipesRepository implements IRecipesRepository {

    private IRecipesDataSource mCacheDataSource;

    @Override
    public Observable<RecipesList> loadRecipes() {
        return getCacheSource()
                .loadRecipes()
                .subscribeOn(Schedulers.io())
                .filter(recipesList -> recipesList != null && !recipesList.isEmpty())
                .switchIfEmpty(RecipesDataSourceFactory.createRemoteDataSource().loadRecipes())
                .doOnNext(this::saveToCacheIfNotEmpty);
    }

    private final void saveToCacheIfNotEmpty(final RecipesList recipesList) {
        if (recipesList != null && !recipesList.isEmpty()) {
            getCacheSource().saveRecipes(recipesList);
            Log.e("TAG", "recipes saved to cache");
        }
    }

    private IRecipesDataSource getCacheSource() {
        if (mCacheDataSource == null) {
            mCacheDataSource = RecipesDataSourceFactory.createMemoryCacheDataSource();
        }

        return mCacheDataSource;
    }

    @Override
    public Observable<Recipe> getRecipe(String id) {
        return getCacheSource()
                .getRecipe(id)
                .subscribeOn(Schedulers.io());
    }

    @Override
    public void release() {
        if (mCacheDataSource != null) {
            mCacheDataSource.release();
        }

        mCacheDataSource = null;
    }
}
