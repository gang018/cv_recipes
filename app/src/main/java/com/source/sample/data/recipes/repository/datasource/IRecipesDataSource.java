package com.source.sample.data.recipes.repository.datasource;
import com.source.sample.data.recipes.model.Recipe;
import com.source.sample.data.recipes.model.RecipesList;
import io.reactivex.Observable;

/**
 * Created by Roman,
 * Date: 21.10.17,
 * email: gang018@mail.ru
 */

public interface IRecipesDataSource {

    Observable<RecipesList> loadRecipes();

    void saveRecipes(final RecipesList list);

    Observable<Recipe> getRecipe(final String id);

    void release();
}
