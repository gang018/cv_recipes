package com.source.sample.data;

/**
 * Created by Roman,
 * Date: 21.10.17,
 * email: gang018@mail.ru
 */

public interface BaseRepository {

    void release();
}
