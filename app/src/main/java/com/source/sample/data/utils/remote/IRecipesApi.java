package com.source.sample.data.utils.remote;
import com.source.sample.data.recipes.model.RecipesList;
import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Headers;

/**
 * Created by: Roman Zakharov
 * Date: 16.10.2017
 * Email: gang018@mail.ru
 */

public interface IRecipesApi {

    String API_KEY = "026e11996a0cc33b052a7924e4f46757";

    @Headers("Content-type: application/json")
    @GET("search?key=" + API_KEY)
    Observable<RecipesList> queryRecipes();
}
