package com.source.sample.data.utils.remote;
import java.util.concurrent.TimeUnit;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.jackson.JacksonConverterFactory;

/**
 * Created by: Roman Zakharov
 * Date: 16.10.2017
 * Email: ri.zakharov@gmail.com
 */
public class ApiManager {

    private static final int TIMEOUT_SEC = 10;

    private static final String RECIPES_BASE_URL = "http://food2fork.com/api/";

    private IRecipesApi mRecipesApi;

    public ApiManager() {
        initCloudApi();
    }

    private final void initCloudApi() {
        final OkHttpClient client = new OkHttpClient.Builder()
                .readTimeout(TIMEOUT_SEC, TimeUnit.SECONDS)
                .connectTimeout(TIMEOUT_SEC, TimeUnit.SECONDS)
                .build();


        final Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(RECIPES_BASE_URL)
                .addConverterFactory(JacksonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(client)
                .build();

        mRecipesApi = retrofit.create(IRecipesApi.class);
    }

    public final IRecipesApi getRecipesApi() {
        return mRecipesApi;
    }

    public final void release()
    {
        mRecipesApi = null;
    }
}