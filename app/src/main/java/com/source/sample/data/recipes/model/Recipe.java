package com.source.sample.data.recipes.model;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by: Roman Zakharov
 * Date: 16.10.2017
 * Email: gang018@mail.ru
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class Recipe {

    public String recipe_id;

    public String publisher;

    public String title;

    public String image_url;

    public float social_rank;

    public String publisher_url;
}
