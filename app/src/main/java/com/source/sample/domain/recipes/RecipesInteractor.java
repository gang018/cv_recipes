package com.source.sample.domain.recipes;
import com.source.sample.data.recipes.model.Recipe;
import com.source.sample.data.recipes.model.RecipesList;
import com.source.sample.data.recipes.repository.IRecipesRepository;
import io.reactivex.Observable;

/**
 * Created by Roman,
 * Date: 22.10.17,
 * email: gang018@mail.ru
 */

public class RecipesInteractor implements IRecipesInteractor {

    private IRecipesRepository mRepository;

    public RecipesInteractor(final IRecipesRepository repository) {
        mRepository = repository;
    }

    @Override
    public Observable<RecipesList> loadRecipes() {
        if (mRepository != null) {
            return mRepository.loadRecipes();
        }

        return null;
    }

    @Override
    public Observable<Recipe> loadRecipe(String id) {
        if (mRepository != null) {
            return mRepository.getRecipe(id);
        }

        return null;
    }

    @Override
    public void release() {
        mRepository = null;
    }
}
