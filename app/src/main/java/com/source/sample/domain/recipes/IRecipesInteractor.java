package com.source.sample.domain.recipes;
import com.source.sample.data.recipes.model.Recipe;
import com.source.sample.data.recipes.model.RecipesList;
import com.source.sample.domain.BaseInteractor;
import io.reactivex.Observable;

/**
 * Created by Roman,
 * Date: 21.10.17,
 * email: gang018@mail.ru
 */

public interface IRecipesInteractor extends BaseInteractor {

    Observable<RecipesList> loadRecipes();

    Observable<Recipe> loadRecipe(final String id);
}
