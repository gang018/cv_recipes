package com.source.sample.domain;

/**
 * Created by Roman,
 * Date: 21.10.17,
 * email: gang018@mail.ru
 */

public interface BaseInteractor {

    void release();
}
