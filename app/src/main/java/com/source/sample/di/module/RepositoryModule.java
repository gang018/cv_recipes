package com.source.sample.di.module;
import com.source.sample.data.recipes.repository.IRecipesRepository;
import com.source.sample.data.recipes.repository.RecipesRepository;
import javax.inject.Singleton;
import dagger.Module;
import dagger.Provides;

/**
 * Created by Roman,
 * Date: 22.10.17,
 * email: gang018@mail.ru
 */

@Module
public class RepositoryModule {

    @Singleton
    @Provides
    public IRecipesRepository provideRecipesRepository() {
        return new RecipesRepository();
    }
}
