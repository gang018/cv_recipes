package com.source.sample.di.component;
import com.source.sample.di.module.RecipesModule;
import com.source.sample.presentation.recipes.card.view.fragment.RecipeCardFragment;
import com.source.sample.presentation.recipes.list.view.fragment.RecipesListFragment;
import dagger.Subcomponent;

/**
 * Created by Roman,
 * Date: 22.10.17,
 * email: gang018@mail.ru
 */

@Subcomponent(modules = RecipesModule.class)
public interface RecipesComponent {

    void inject(RecipesListFragment fragment);

    void inject(RecipeCardFragment fragment);
}
