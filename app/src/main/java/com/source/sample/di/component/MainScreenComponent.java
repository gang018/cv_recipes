package com.source.sample.di.component;
import com.source.sample.di.module.MainScreenModule;
import com.source.sample.presentation.mainscreen.view.activity.MainScreenActivity;
import dagger.Subcomponent;

/**
 * Created by: Roman Zakharov
 * Date: 24.10.2017
 * Email: gang018@mail.ru
 */

@Subcomponent(modules = MainScreenModule.class)
public interface MainScreenComponent {

    void inject(MainScreenActivity activity);
}
