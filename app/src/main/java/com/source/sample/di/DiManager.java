package com.source.sample.di;
import com.source.sample.di.component.DaggerRepositoryComponent;
import com.source.sample.di.component.MainScreenComponent;
import com.source.sample.di.component.RecipesComponent;
import com.source.sample.di.component.RepositoryComponent;
import com.source.sample.di.component.RouterComponent;
import com.source.sample.di.module.MainScreenModule;
import com.source.sample.di.module.RecipesModule;
import com.source.sample.di.module.RouterModule;

/**
 * Created by Roman,
 * Date: 22.10.17,
 * email: gang018@mail.ru
 */

public class DiManager {

    private static RepositoryComponent sRepositoryComponent;
    private static RecipesComponent sRecipesComponent;
    private static MainScreenComponent sMainScreenComponent;
    private static RouterComponent sRouterComponent;

    public DiManager() {

    }

    public static final RepositoryComponent getRepositoryComponent() {
        if (sRepositoryComponent == null) {
            sRepositoryComponent = DaggerRepositoryComponent.create();
        }

        return sRepositoryComponent;
    }

    public static final RouterComponent getRouterComponent() {
        if (sRouterComponent == null) {
            sRouterComponent = getRepositoryComponent().plus(new RouterModule());
        }

        return sRouterComponent;
    }

    public static final RecipesComponent getRecipesComponent() {
        if (sRecipesComponent == null) {
            sRecipesComponent = getRouterComponent().plus(new RecipesModule());
        }

        return sRecipesComponent;
    }

    public static final MainScreenComponent getMainScreenComponent() {
        if (sMainScreenComponent == null) {
            sMainScreenComponent = getRouterComponent().plus(new MainScreenModule());
        }
        return sMainScreenComponent;
    }

    public static void release() {
        sRecipesComponent = null;
        sRepositoryComponent = null;
        sMainScreenComponent = null;
        sRouterComponent = null;
    }
}
