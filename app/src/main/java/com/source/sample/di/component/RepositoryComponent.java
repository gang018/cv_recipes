package com.source.sample.di.component;
import com.source.sample.di.module.RepositoryModule;
import com.source.sample.di.module.RouterModule;
import javax.inject.Singleton;
import dagger.Component;

/**
 * Created by Roman,
 * Date: 22.10.17,
 * email: gang018@mail.ru
 */

@Singleton
@Component(modules = RepositoryModule.class)
public interface RepositoryComponent {

    RouterComponent plus(final RouterModule module);
}
