package com.source.sample.di.module;
import com.source.sample.presentation.mainscreen.presenter.IMainScreenPresenter;
import com.source.sample.presentation.mainscreen.presenter.MainScreenPresenter;
import dagger.Module;
import dagger.Provides;

/**
 * Created by: Roman Zakharov
 * Date: 24.10.2017
 * Email: gang018@mail.ru
 */

@Module
public class MainScreenModule {

    @Provides
    public IMainScreenPresenter provideMainScreenPresenter() {
        return new MainScreenPresenter();
    }
}
