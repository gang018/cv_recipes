package com.source.sample.di.module;
import com.source.sample.data.recipes.repository.IRecipesRepository;
import com.source.sample.domain.recipes.IRecipesInteractor;
import com.source.sample.domain.recipes.RecipesInteractor;
import com.source.sample.presentation.recipes.card.presenter.IRecipeCardPresenter;
import com.source.sample.presentation.recipes.card.presenter.RecipeCardPresenter;
import com.source.sample.presentation.recipes.list.presenter.IRecipesPresenter;
import com.source.sample.presentation.recipes.list.presenter.RecipesPresenter;
import dagger.Module;
import dagger.Provides;

/**
 * Created by Roman,
 * Date: 22.10.17,3
 * email: gang018@mail.ru
 */

@Module
public class RecipesModule {

    @Provides
    public IRecipesPresenter provideRecipesPresenter(final IRecipesInteractor interactor) {
        return new RecipesPresenter(interactor);
    }

    @Provides
    public IRecipesInteractor provideRecipesInteractor(final IRecipesRepository repository) {
        return new RecipesInteractor(repository);
    }

    @Provides
    public IRecipeCardPresenter provideRecipeCardPresenter(final IRecipesInteractor interactor) {
        return new RecipeCardPresenter(interactor);
    }
}
