package com.source.sample.di.component;
import com.source.sample.di.module.MainScreenModule;
import com.source.sample.di.module.RecipesModule;
import com.source.sample.di.module.RouterModule;
import com.source.sample.di.scope.RouterScope;
import dagger.Subcomponent;

/**
 * Created by: Roman Zakharov
 * Date: 03.11.2017
 * Email: gang018@mail.ru
 */

@RouterScope
@Subcomponent(modules = RouterModule.class)
public interface RouterComponent {

    RecipesComponent plus(final RecipesModule module);

    MainScreenComponent plus(final MainScreenModule module);
}
