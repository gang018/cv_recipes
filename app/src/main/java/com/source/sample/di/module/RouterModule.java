package com.source.sample.di.module;
import com.source.sample.di.scope.RouterScope;
import com.source.sample.presentation.router.AppScreensRouter;
import dagger.Module;
import dagger.Provides;

/**
 * Created by: Roman Zakharov
 * Date: 03.11.2017
 * Email: gang018@mail.ru
 */

@Module
public class RouterModule {

    @RouterScope
    @Provides
    public AppScreensRouter provideRouter() {
        return new AppScreensRouter();
    }
}
