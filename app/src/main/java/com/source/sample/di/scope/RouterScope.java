package com.source.sample.di.scope;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import javax.inject.Scope;

/**
 * Created by: Roman Zakharov
 * Date: 03.11.2017
 * Email: gang018@mail.ru
 */

@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface RouterScope {
}
